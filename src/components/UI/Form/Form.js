import React from 'react';
import './Form.css';

const Form = props => {
    return (
        <form className='InputForm'>
            <label htmlFor="title">Title:</label>
            <input id='title' type="text" value={props.title} placeholder='Enter title' onChange={props.changeTitle}/>
            <label htmlFor="description">Description:</label>
            <textarea id='desciption' type="text" value={props.description} placeholder='Enter description' onChange={props.enterDescription}/>
        </form>
    );
};

export default Form;