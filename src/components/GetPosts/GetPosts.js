import React, {Component, Fragment} from 'react';
import axios from '../../axiosBase';

import './GetPosts.css';

import Button from '../../components/UI/Button/Button';
import Navigation from '../../components/Navigation/Navigation';

class GetPosts extends Component {

  state = {
    names: [],
  };

  componentDidMount () {

    axios.get('posts.json').then(response => {
      const name = [];
      for (let key in response.data) {
        const id = key;
        const title = response.data[key].title;
        const description = response.data[key].description;
        const titles = {id: id, title: title, description: description};
        name.push(titles);
      }

      this.setState({names: name})
    });
  };


  editPost = index => {
    this.props.history.push('/posts/' + index);
  };

  render() {
    const posts = this.state.names.reverse().map((item, index) => {
      return (
        <div key={index} className='Posts'>
          <p className='titlePosts'><span>Title:</span> {item.title}</p>
          <Button
            btnType="Success"
            onClick={() => this.editPost(item.id)}
          >
            Read more >>>
          </Button>
        </div>
      )
    });
    return (
      <Fragment>
        <Navigation/>
        <div className="BackGroundBlog">
          <div className='blog'>
            {posts}
          </div>
        </div>
      </Fragment>
    );
  }
}

export default GetPosts;