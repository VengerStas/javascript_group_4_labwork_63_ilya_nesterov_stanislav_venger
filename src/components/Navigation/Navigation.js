import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

import './Navigation.css';

class Navigation extends Component {
  render() {
    return (
        <div className='homepage'>
            <NavLink to='/'>My Blog</NavLink>
            <NavLink to="/">Home</NavLink>
            <NavLink to="/addpost">Add Post</NavLink>
            <NavLink to="/about">About Ilya</NavLink>
            <NavLink to="/about-two">About Stas</NavLink>
            <NavLink to="/contacts">Contacts</NavLink>
        </div>
    );
  }
}

export default Navigation;