import React, {Component, Fragment} from 'react';

import Navigation from "../../components/Navigation/Navigation";
import Button from "../../components/UI/Button/Button";
import axios from "../../axiosBase";
import Form from "../UI/Form/Form";

class EditPost extends Component {

  state = {
    title: '',
    description: '',
  };

  changeTitle = event => {
    this.setState({title: event.target.value});
  };

  enterDescription = event => {
    this.setState({description: event.target.value})
  };

  componentDidMount() {
    axios.get('posts/' + this.props.match.params.id + '.json').then(response => {
      const title = response.data.title;
      const description = response.data.description;
      this.setState({title, description})
    })
  }

  editPost = event => {
    event.preventDefault();

    const post = {
      title: this.state.title,
      description: this.state.description
    };

    axios.put('posts/' + this.props.match.params.id + '.json', post).then(() => {
      this.props.history.push('/')
    });
  };

  render() {
    return (
      <Fragment>
        <Navigation/>
        <div className="background">
            <Form
                title={this.state.title}
                description={this.state.description}
                changeTitle={this.changeTitle}
                enterDescription={this.enterDescription}
            />
            <Button
              btnType='Add'
              onClick={this.editPost}
            >
            Edit post
            </Button>
        </div>
      </Fragment>
    );
  }
}

export default EditPost;