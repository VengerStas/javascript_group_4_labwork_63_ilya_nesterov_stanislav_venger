import React, {Component, Fragment} from 'react';

import './Contacts.css';

import Navigation from '../../components/Navigation/Navigation';

class Contacts extends Component {
  render() {
    return (
      <Fragment>
        <Navigation/>
        <div className='contacts'>
          <div className="aboutme">
            <strong>Address:</strong>
            <p>Tampa, 10006 N Dale Mabry Hwy</p>
            <hr/>
            <strong>Phone number:</strong>
            <p>Phone: (727) 545-4654</p>
            <hr/>
          </div>
          <div className="contact">
            <strong>Contacts:</strong>
            <strong>Skype:</strong> <p>SkypeAccount</p>
            <strong>Аgent:</strong> <p>test@gmail.com</p>
            <strong>Instagram:</strong> <p>Instagram Info</p>
            <strong>WhatsApp:</strong> <p>(727) 545-4654</p>
            <strong>Telegram:</strong> <p>(727) 545-4654</p>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Contacts;