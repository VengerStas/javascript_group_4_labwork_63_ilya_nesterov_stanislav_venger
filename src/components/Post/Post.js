import React, {Component, Fragment} from 'react';
import axios from '../../axiosBase';

import './Post.css';

import Button from "../../components/UI/Button/Button";
import Navigation from '../../components/Navigation/Navigation';


class EditPost extends Component {

  state = {
    title: '',
    description: '',
  };

  componentDidMount() {
    axios.get('posts/' + this.props.match.params.id + '.json').then(response => {
      this.setState({title: response.data.title, description: response.data.description})
    });
  }

  deleteDescription = () => {
    axios.delete('posts/' + this.props.match.params.id + '.json').then(() => {
      this.props.history.push('/');
    });
  };

  editDescription = () => {
    this.props.history.push(this.props.match.params.id + '/edit');
  };

  render () {
    return (
      <Fragment>
        <Navigation/>
        <div className="bgpost">
          <div className='post'>
            <p className='titlePosts'><span>Title: </span>{this.state.title}</p>
            <p className='titlePosts'><span>Description: </span>{this.state.description}</p>
            <Button
              btnType='Success'
              onClick={this.editDescription}
            >
              Edit Message
            </Button>
            <Button
              btnType='Danger'
              onClick={this.deleteDescription}
            >
              Delete
            </Button>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default EditPost;