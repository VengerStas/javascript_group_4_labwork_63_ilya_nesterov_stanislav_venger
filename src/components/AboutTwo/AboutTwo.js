import React, {Component, Fragment} from 'react';
import Navigation from "../Navigation/Navigation";
import Secret from '../../assets/secret.jpg';
import './AboutTwo.css';

class AboutTwo extends Component {
    render() {
        return (
           <Fragment>
               <Navigation/>
               <div className="about-block-2">
                   <img src={Secret} alt="Secret-information"/>
               </div>
           </Fragment>
        );
    }
}

export default AboutTwo;