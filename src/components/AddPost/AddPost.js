import React, {Component, Fragment} from 'react';
import axios from '../../axiosBase';

import './AddPost.css';

import Navigation from '../../components/Navigation/Navigation';
import Button from '../../components/UI/Button/Button';
import Form from "../UI/Form/Form";

class AddPost extends Component {

  state = {
    title: '',
    description: '',
  };

  changeTitle = event => {
    this.setState({title: event.target.value});
  };

  enterDescription = event => {
    this.setState({description: event.target.value})
  };

  addPost = event => {
  event.preventDefault();

  const post = {
    title: this.state.title,
    description: this.state.description
  };

  axios.post('posts.json', post).then(() => {
    this.props.history.push('/')
  });
  };

  render() {
    return (
      <Fragment>
        <Navigation/>
        <div className="background">
            <Form
                title={this.state.title}
                description={this.state.description}
                changeTitle={this.changeTitle}
                enterDescription={this.enterDescription}
              />
            <Button
                btnType='Add'
                onClick={this.addPost}
            >Add post
            </Button>
        </div>
      </Fragment>
    );
  }
}

export default AddPost;