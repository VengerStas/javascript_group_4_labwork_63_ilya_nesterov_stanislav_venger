import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://blogpost-94d01.firebaseio.com/'
});

export default instance;