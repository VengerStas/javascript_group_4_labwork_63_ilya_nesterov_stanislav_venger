import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";

import './App.css';

import GetPosts from '../components/GetPosts/GetPosts';
import Post from '../components/Post/Post';
import About from '../components/About/About';
import AddPost from '../components/AddPost/AddPost';
import Contacts from '../components/Contacts/Contacts';
import EditPost from '../components/EditPost/EditPost';
import AboutTwo from "../components/AboutTwo/AboutTwo";

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Switch>
                <Route path="/" exact component={GetPosts} />
                <Route path="/addpost" component={AddPost} />
                <Route path="/posts/:id/edit" component={EditPost} />
                <Route path="/posts/:id" component={Post} />
                <Route path="/about" component={About} />
                <Route path="/about-two" component={AboutTwo}/>
                <Route path="/contacts" component={Contacts} />
                <Route render={() => <h1>Not found</h1>} />
          </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
